<?php

declare(strict_types=1);

namespace App\Factories;

use App\Components\Config;
use App\Exceptions\InvalidRequestException;
use App\Interfaces\Factory\ConfigFactoryInterface;
use Illuminate\Http\Request;

/**
 * Class ConfigFactory
 *
 * @package App\Factories
 */
class ConfigFactory implements ConfigFactoryInterface
{
    /**
     * @param Request $request
     *
     * @return Config
     * @throws InvalidRequestException
     */
    public function createFromRequest(Request $request): Config
    {
        $config = new Config();
        $start = $request->input(Config::PARAM_START);
        if (empty($start)) {
            $config->setX((int)$request->session()->get(Config::SESSION_STORE_X));
            $config->setY((int)$request->session()->get(Config::SESSION_STORE_Y));
        }
        $commands = $request->input(Config::PARAM_COMMANDS);

        if(empty($commands)) {
            throw new InvalidRequestException('Commands not provided');
        }

        $config->setCommands(array_map('strval', explode(',', $commands)));

        if (!empty($start)) {
            $coords = array_map('intval', explode('x', $start));
            if (
                !is_array($coords)
                || count($coords) != 2
                || $coords[0] < 0
                || $coords[0] > $config->getMaxX()
                || $coords[1] < 0
                || $coords[1] > $config->getMaxY()
            ) {
                throw new InvalidRequestException('Invalid start position provided');
            }
            $config->setX($coords[0]);
            $config->setY($coords[1]);
        }
        $config->setHasWalls(filter_var($request->input(Config::PARAM_HAS_WALLS), FILTER_VALIDATE_BOOLEAN));
        return $config;
    }
}
