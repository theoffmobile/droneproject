<?php

declare(strict_types=1);

namespace App\Services;

use App\Components\Command;
use App\Components\Config;
use App\Components\Drone;
use App\Components\Position;
use App\Components\Result;
use App\Exceptions\DroneStuckException;
use App\Exceptions\GoneAwayException;
use Assert\Assertion;
use Exception;
use Illuminate\Support\Facades\Session;
use Psr\Log\LoggerInterface;

/**
 * Class SimulationService
 *
 * @package App\Services
 */
class SimulationService
{
    /** @var Config */
    private $config;

    /** @var LoggerInterface */
    private $logger;

    /** @var Result */
    private $result;

    /** @var Position */
    private $position;

    /** @var Drone */
    private $drone;

    /**
     * SimulationService constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->result = new Result();
        $this->position = new Position();
        $this->drone = new Drone();
    }

    /**
     * @param Config $config
     *
     * @return Result
     * @throws Exception
     */
    public function simulate(Config $config)
    {
        $this->config = $config;
        $this->position->x = $config->getX();
        $this->position->y = $config->getY();
        try {
            $this->handleQueue($config->getCommands());
            Session::put(Config::SESSION_STORE_X, $this->position->x);
            Session::put(Config::SESSION_STORE_Y,  $this->position->y);
            Session::save();
            return $this->result;
        } catch (Exception $ex) {
            $this->logger->error($ex->getMessage());
            throw $ex;
        }
    }

    /**
     * @param array $commands
     *
     * @throws DroneStuckException
     * @throws GoneAwayException
     */
    private function handleQueue(array $commands): void
    {
        Assertion::allString($commands);
        foreach ($commands as $command) {
            Command::assertValidValue($command);
            $this->move($command);
        }
    }

    /**
     * @param string $command
     *
     * @throws DroneStuckException
     * @throws GoneAwayException
     */
    private function move(string $command) {
        $moveSet = $this->drone->getMove($command);
        $this->position->x = $this->position->x + $moveSet[0];
        $this->position->y = $this->position->y + $moveSet[1];
        if (!$this->checkPosition()) {
            if ($this->config->isHasWalls()) {
                $backOff = $this->drone->getBackoffMove($command);
                $this->position->x = $this->position->x + $backOff[0];
                $this->position->y = $this->position->y + $backOff[1];
                if (!$this->checkPosition()) {
                    throw new DroneStuckException('The drone have been stuck');
                }
            } else {
                throw new GoneAwayException('The drone has gone away');
            }
        }
        $this->result->logHistory($this->position);
    }

    /**
     * @return bool
     */
    private function checkPosition(): bool
    {
        if (
            $this->position->x < 0
            || $this->position->y < 0
            || $this->position->x > $this->config->getMaxX()
            || $this->position->y > $this->config->getMaxY()
        ) {
            return false;
        }
        return true;
    }
}
