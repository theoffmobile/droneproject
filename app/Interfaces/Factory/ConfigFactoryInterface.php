<?php

declare(strict_types=1);

namespace App\Interfaces\Factory;

use App\Components\Config;
use Illuminate\Http\Request;

/**
 * Interface ConfigFactoryInterface
 */
interface ConfigFactoryInterface
{
    public function createFromRequest(Request $request): Config;
}
