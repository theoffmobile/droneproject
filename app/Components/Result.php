<?php

declare(strict_types=1);

namespace App\Components;

/**
 * Class Result
 *
 * @package App\Components
 */
class Result
{
    /** @var int */
    public $x;

    /** @var int */
    public $y;

    /** @var Position[] */
    public $history = [];

    /**
     * @param Position $position
     */
    public function logHistory(Position $position) {
        $this->x = $position->x;
        $this->y = $position->y;
        // Because it's not immutable
        $this->history[] = clone $position;
    }
}
