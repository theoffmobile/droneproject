<?php

declare(strict_types=1);

namespace App\Components;

use MyCLabs\Enum\Enum;

/**
 * Class Command
 *
 * @package App\Components
 */
class Command extends Enum
{
    public const UP = 'up';
    public const DOWN = 'down';
    public const LEFT = 'left';
    public const RIGHT = 'right';
}
