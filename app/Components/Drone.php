<?php

declare(strict_types=1);

namespace App\Components;

/**
 * Class Drone
 *
 * @package App\Components
 */
class Drone
{
    /**
     * @param string $move
     *
     * @return array
     */
    public function getMove(string $move): array
    {
        switch ($move) {
            case Command::UP:
                return [0, 1];
            case Command::DOWN:
                return [0, -1];
            case Command::LEFT:
                return [-1, 0];
            case Command::RIGHT:
                return [1, 0];
            default:
                return [0, 0];
        }
    }

    /**
     * @param string $move
     *
     * @return array
     */
    public function getBackoffMove(string $move): array
    {
        switch ($move) {
            case Command::UP:
                return [0, -2];
            case Command::DOWN:
                return [0, 2];
            case Command::LEFT:
                return [2, 0];
            case Command::RIGHT:
                return [-2, 0];
            default:
                return [0, 0];
        }
    }
}
