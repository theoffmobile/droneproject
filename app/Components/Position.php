<?php

declare(strict_types=1);

namespace App\Components;

/**
 * Class Position
 *
 * @package App\Components
 */
class Position
{
    /** @var int */
    public $x;

    /** @var int */
    public $y;
}
