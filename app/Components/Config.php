<?php

declare(strict_types=1);

namespace App\Components;

/**
 * Class Config
 */
class Config
{
    /** @var string */
    public const SESSION_STORE_X = 'drone_start_x';

    /** @var string */
    public const SESSION_STORE_Y = 'drone_start_x';

    /** @var string */
    public const PARAM_START = 'start_position';

    /** @var string */
    public const PARAM_COMMANDS = 'commands';

    /** @var string */
    public const PARAM_HAS_WALLS = 'has_walls';

    /** @var int */
    private $x = 0;

    /** @var int */
    private $y = 0;

    /** @var int */
    private $maxX = 99;

    /** @var int */
    private $maxY = 99;

    /** @var string[] */
    private $commands;

    /** @var bool */
    private $hasWalls = false;

    /**
     * @return int
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @param int $x
     */
    public function setX(int $x): void
    {
        $this->x = $x;
    }

    /**
     * @return int
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * @param int $y
     */
    public function setY(int $y): void
    {
        $this->y = $y;
    }

    /**
     * @return string[]
     */
    public function getCommands(): array
    {
        return $this->commands;
    }

    /**
     * @param string[] $commands
     */
    public function setCommands(array $commands): void
    {
        $this->commands = $commands;
    }

    /**
     * @return bool
     */
    public function isHasWalls(): bool
    {
        return $this->hasWalls;
    }

    /**
     * @param bool $hasWalls
     */
    public function setHasWalls(bool $hasWalls): void
    {
        $this->hasWalls = $hasWalls;
    }

    /**
     * @return int
     */
    public function getMaxX(): int
    {
        return $this->maxX;
    }

    /**
     * @param int $maxX
     */
    public function setMaxX(int $maxX): void
    {
        $this->maxX = $maxX;
    }

    /**
     * @return int
     */
    public function getMaxY(): int
    {
        return $this->maxY;
    }

    /**
     * @param int $maxY
     */
    public function setMaxY(int $maxY): void
    {
        $this->maxY = $maxY;
    }
}
