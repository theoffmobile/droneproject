<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

/**
 * Class DroneStuckException
 *
 * @package App\Exceptions
 */
class DroneStuckException extends Exception
{

}
