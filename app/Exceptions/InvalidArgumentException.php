<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

/**
 * Class InvalidArgumentException
 *
 * @package App\Exceptions
 */
class InvalidArgumentException extends Exception
{

}
