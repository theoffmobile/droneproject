<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

/**
 * Class GoneAwayException
 *
 * @package App\Exceptions
 */
class GoneAwayException extends Exception
{

}
