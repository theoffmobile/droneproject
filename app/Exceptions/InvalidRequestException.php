<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;

/**
 * Class InvalidRequestException
 *
 * @package App\Exceptions
 */
class InvalidRequestException extends Exception
{

}
