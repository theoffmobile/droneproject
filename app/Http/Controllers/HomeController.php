<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Exceptions\InvalidRequestException;
use App\Factories\ConfigFactory;
use App\Interfaces\Factory\ConfigFactoryInterface;
use App\Services\SimulationService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Class HomeController
 *
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /** @var ConfigFactoryInterface */
    private $factory;

    /** @var SimulationService */
    private $service;

    /**
     * HomeController constructor.
     *
     * @param ConfigFactory $factory
     * @param SimulationService $service
     */
    public function __construct(ConfigFactory $factory, SimulationService $service)
    {
        $this->factory = $factory;
        $this->service = $service;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws InvalidRequestException
     */
    public function index(Request $request)
    {
        $config = $this->factory->createFromRequest($request);
        try {
            return new JsonResponse($this->service->simulate($config));
        } catch (Exception $ex) {
            return new JsonResponse(
                ['error' => $ex->getMessage(),],
                500
            );
        }
    }
}
